import { TestBed } from '@angular/core/testing';

import { DetailGameService } from './detail-game.service';

describe('DetailGameService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DetailGameService = TestBed.get(DetailGameService);
    expect(service).toBeTruthy();
  });
});

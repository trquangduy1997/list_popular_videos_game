import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DetailGameService } from './detail-game.service';

@Component({
  selector: 'app-detail-game',
  templateUrl: './detail-game.component.html',
  styleUrls: ['./detail-game.component.css']
})
export class DetailGameComponent implements OnInit {
  id : string;
  game : any;

  constructor(private route : ActivatedRoute, private detailService : DetailGameService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.detailService.getDetailGame(this.id).subscribe((response) => {
      // console.log(response);
      this.game = response;
    })
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ListGameService } from './list-game.service';

@Component({
  selector: 'app-list-game',
  templateUrl: './list-game.component.html',
  styleUrls: ['./list-game.component.css']
})
export class ListGameComponent implements OnInit {
  items = [];
  pageOfItems: Array<any>;

  constructor(private listGameService : ListGameService, private router : Router) { }

  ngOnInit() {
    this.listGameService.getTopGames().subscribe((response) => {
      this.items = response.results;
      // console.log(this.items);
    });
  }

  onChangePage(pageOfItems : Array<any>){
    this.pageOfItems = pageOfItems;
  }

  showGameDetails(id : string) {
    let url = "/" + id;
    this.router.navigateByUrl(url);
  }
}

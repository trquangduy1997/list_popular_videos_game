import { TestBed } from '@angular/core/testing';

import { ListGameService } from './list-game.service';

describe('ListGameService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListGameService = TestBed.get(ListGameService);
    expect(service).toBeTruthy();
  });
});

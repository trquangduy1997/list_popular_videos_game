import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListGameService {
  private apiKey : string;

  constructor(private http : HttpClient){ 
    this.apiKey = "d34fdc359be6446f8e3be9e468464737";
   }

  getTopGames() : Observable<any> {
    let url = "http://localhost:4200/api/games?dates=2019-01-01,2019-12-31&ordering=-added&key=" + this.apiKey;
    return this.http.get<any>(
      url,
      { withCredentials: true }
      );
  }
}

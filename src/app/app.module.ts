import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JwPaginationComponent } from 'jw-angular-pagination';
import { ListGameComponent } from './list-game/list-game.component';
import { HttpClientModule } from '@angular/common/http';
import { DetailGameComponent } from './detail-game/detail-game.component';

@NgModule({
  declarations: [
    AppComponent,
    JwPaginationComponent,
    ListGameComponent,
    DetailGameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

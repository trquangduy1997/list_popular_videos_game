import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailGameComponent } from './detail-game/detail-game.component';
import { ListGameComponent } from './list-game/list-game.component';

const routes: Routes = [
  { path: '', component: ListGameComponent },
  { path: ':id', component: DetailGameComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
